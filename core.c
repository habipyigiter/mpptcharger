
#include <xc.h>
#include "core.h"

// FBS
#pragma config BWRP = WRPROTECT_OFF     // Boot Segment Write Protect (Boot Segment may be written)
#pragma config BSS = NO_FLASH           // Boot Segment Program Flash Code Protection (No Boot program Flash segment)

// FGS
#pragma config GWRP = OFF               // General Code Segment Write Protect (General Segment may be written)
#pragma config GSS = OFF                // General Segment Code Protection (User program memory is not code-protected)

// FOSCSEL
//#pragma config FNOSC = FRC              // Oscillator Source Selection (Internal Fast RC (FRC) oscillator)
#pragma config FNOSC = FRCPLL           // Oscillator Source Selection (Internal Fast RC (FRC) with PLL)
#pragma config IESO = ON                // Internal External Switch Over Mode (Start up device with FRC, then automatically switch to user-selected oscillator source)

// FOSC
#pragma config POSCMD = NONE            // Primary Oscillator Source (Primary oscillator disabled)
//#pragma config OSCIOFNC = OFF           // OSC2 Pin Function (OSC2 is clock output)
#pragma config OSCIOFNC = ON            // OSC2 Pin Function (OSC2 is general purpose digital I/O pin)
#pragma config IOL1WAY = ON             // Peripheral Pin Select Configuration (Allow only one reconfiguration)
//#pragma config IOL1WAY = OFF            // Peripheral Pin Select Configuration (Allow multiple reconfigurations)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor (Clock switching and Fail-Safe Clock Monitor are disabled, Mon Disabled)

// FWDT
#pragma config WDTPOST = PS32768        // Watchdog Timer Postscaler (1:32,768)
#pragma config WDTPRE = PR128           // WDT Prescaler (1:128)
#pragma config WINDIS = OFF             // Watchdog Timer Window (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = OFF              // Watchdog Timer Enable (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR128           // POR Timer Value (128ms)

// FICD
#pragma config ICS = PGD2               // Comm Channel Select (Communicate on PGC1/EMUC1 and PGD1/EMUD1)
#pragma config JTAGEN = OFF             // JTAG Port Enable (JTAG is disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

void CORE_InitCore(void) {
    
    /* Configure Oscillator to operate the device at 40Mhz
	   Fosc= Fin*M/(N1*N2), Fcy=Fosc/2
 	   Fosc= 7.37*(32)/(2*2)=64Mhz for Fosc, Fcy = 32Mhz */

	/* Configure PLL prescaler, PLL postscaler, PLL divisor */
	PLLFBD=30; 				/* M = PLLFBD + 2 */
	CLKDIVbits.PLLPOST=0;   /* N1 = 2 */
	CLKDIVbits.PLLPRE=0;    /* N2 = 2 */

    __builtin_write_OSCCONH(0x01);			/* New Oscillator FRC w/ PLL */
    __builtin_write_OSCCONL(0x01);  		/* Enable Switch */
      
	while(OSCCONbits.COSC != 0b001);		/* Wait for new Oscillator to become FRC w/ PLL */  
    while(OSCCONbits.LOCK != 1);			/* Wait for Pll to Lock */

	/* Now setup the ADC and PWM clock for 120MHz
	   ((FRC * 16) / APSTSCLR ) = (7.37 * 16) / 1 = ~ 120MHz*/

	ACLKCONbits.FRCSEL = 1;					/* FRC provides input for Auxiliary PLL (x16) */
	ACLKCONbits.SELACLK = 1;				/* Auxiliary Oscillator provides clock source for PWM & ADC */
	ACLKCONbits.APSTSCLR = 7;				/* Divide Auxiliary clock by 1 */
	ACLKCONbits.ENAPLL = 1;					/* Enable Auxiliary PLL */
	
	while(ACLKCONbits.APLLCK != 1);			/* Wait for Auxiliary PLL to Lock */
    
    
//    
//    OSCCONbits.COSC = 0; //000 = Fast RC oscillator (FRC)
//    OSCCONbits.NOSC = 0; //000 = Fast RC oscillator (FRC)

    OSCTUNbits.TUN = 17;

}

void CORE_InitPorts(void) {

    ADPCFG = 0xFFFF;

    TRISCbits.TRISC2 = 0; // LED1
    TRISCbits.TRISC7 = 0; // LED2
    TRISCbits.TRISC8 = 0; // LED3

    /* PWM2 */
    TRISBbits.TRISB13 = 0; /* Set as a digital output  PWM2 */
    TRISBbits.TRISB14 = 0; /* Set as a digital output  PWM2 */
    LATBbits.LATB13 = 1;
    LATBbits.LATB14 = 1;

    /*PWM1*/
    TRISAbits.TRISA3 = 0; /* Set as a digital output  PWM1 */
    TRISAbits.TRISA4 = 0; /* Set as a digital output  PWM1 */
    LATAbits.LATA3 = 0;
    LATAbits.LATA4 = 0;

    /*ST7565 LCD DIRECTION*/
    TRISCbits.TRISC5 = 0; // LCD AO
    TRISCbits.TRISC3 = 0; // LCD CS1
    TRISCbits.TRISC6 = 0; // LCD RES   
    TRISCbits.TRISC4 = 0; // LCD D6
    TRISBbits.TRISB7 = 0; // LCD D7   

    LATCbits.LATC5 = 0;
    LATCbits.LATC3 = 0;
    LATCbits.LATC6 = 0;
    LATCbits.LATC4 = 0;
    LATBbits.LATB7 = 0;
    
    /*ADC PIN DIRECTION*/
    
    ADPCFGbits.PCFG0=0;  // RA0 PIN ANALOG
    TRISAbits.TRISA0=1;  //INPUT RA0
    
    ADPCFGbits.PCFG1=0;  // RA0 PIN ANALOG
    TRISAbits.TRISA1=1;  //INPUT RA1
    
//    ADPCFGbits.PCFG10=0; // RC10 PIN ANALOG SONT YUK
//    TRISCbits.TRISC10=1;    // INPUT
    
    ADPCFGbits.PCFG11=0;
    TRISCbits.TRISC9=1;
    
    
    /*UART */
    TRISCbits.TRISC0=0;     //RC0 OUTPUT TX RP16
    
    _RP16R=3;
    


}