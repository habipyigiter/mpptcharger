/**************************************************************************************
*  � 2008 Microchip Technology Inc.                                         
*                                                                           
*  FileName:              True Independent PWM Output.c                                    
*  Dependencies:          Header (.h) files if applicable, see below        
*  Processor:             dsPIC33FJ16GS502                                  
*  Compiler:              MPLAB� C30 v3.02 or higher                        
*  IDE:                   MPLAB� IDE v8.10 or later                         
*  Hardware Dependencies: 16-Bit 28-Pin Starter Board			             
*                                                                           
*  SOFTWARE LICENSE AGREEMENT:                                              
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip's
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP'S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*                                                                         
*****************************************************************************
* Description:                                                              *
*   This program illustrates the use of the PWM module in the true 			*
* independent output mode. PWM1H and PWM1L have different switching 		*
* frequencies but the same duty cycle (50% of switching period).			*
*                                                                           *                
*****************************************************************************/
#include "p33FJ16GS502.h"

/* Configuration Bit Settings */
_FOSCSEL(FNOSC_FRC)
_FOSC(FCKSM_CSECMD & OSCIOFNC_ON)
_FWDT(FWDTEN_OFF)
_FPOR(FPWRT_PWR128)
_FICD(ICS_PGD2 & JTAGEN_OFF)

void init_PWM(void);

int main()
{
/* Configure Oscillator to operate the device at 40Mhz
	   Fosc= Fin*M/(N1*N2), Fcy=Fosc/2
 	   Fosc= 7.37*(43)/(2*2)=80Mhz for Fosc, Fcy = 40Mhz */

	/* Configure PLL prescaler, PLL postscaler, PLL divisor */
	PLLFBD=41; 				/* M = PLLFBD + 2 */
	CLKDIVbits.PLLPOST=0;   /* N1 = 2 */
	CLKDIVbits.PLLPRE=0;    /* N2 = 2 */

    __builtin_write_OSCCONH(0x01);			/* New Oscillator FRC w/ PLL */
    __builtin_write_OSCCONL(0x01);  		/* Enable Switch */
      
	while(OSCCONbits.COSC != 0b001);		/* Wait for new Oscillator to become FRC w/ PLL */  
    while(OSCCONbits.LOCK != 1);			/* Wait for Pll to Lock */

	/* Now setup the ADC and PWM clock for 120MHz
	   ((FRC * 16) / APSTSCLR ) = (7.37 * 16) / 1 = ~ 120MHz*/

	ACLKCONbits.FRCSEL = 1;					/* FRC provides input for Auxiliary PLL (x16) */
	ACLKCONbits.SELACLK = 1;				/* Auxiliary Oscillator provides clock source for PWM & ADC */
	ACLKCONbits.APSTSCLR = 7;				/* Divide Auxiliary clock by 1 */
	ACLKCONbits.ENAPLL = 1;					/* Enable Auxiliary PLL */
	
	while(ACLKCONbits.APLLCK != 1);			/* Wait for Auxiliary PLL to Lock */


    init_PWM();
    

    while(1);                     			/* Infinite Loop */
}

void init_PWM()
{

    PHASE1 = 1920;                          /* PWM1H, PHASEx = ((1 / 500kHz) / 1.04ns) = 1923, where 500kHz 
											 is the desired switching frequency and 1.04ns is PWM resolution. */

    SPHASE1 = 3840;                         /* PWM1L, SPHASEx = ((1 / 250kHz) / 1.04ns) = 3846, where 250kHz 
											 is the desired switching frequency and 1.04ns is PWM resolution. */

	PWMCON1bits.ITB = 1;					/* Use Phasex and SPHASEx registers for Independent time base */
	PWMCON1bits.DTC = 2;					/* Disable Deadtime */

   
    IOCON1bits.PENH = 1;      				/* PWM1H is controlled by PWM module */ 
    IOCON1bits.PENL = 1;      				/* PWM1L is controlled by PWM module */
    IOCON1bits.PMOD = 3;      				/* Select True Independent Output PWM mode */

    PDC1 = 960;					   			/* 50% Duty cycle for PWM1H */ 
	SDC1 = 1920;							/* 50% Duty cycle for PWM1L */
    
    PTCONbits.PTEN       = 1;      			/* Enable the PWM Module */
}
