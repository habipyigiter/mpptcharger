                     		Readme File for Code Example:
               CE161 - True Independent PWM Output Mode using dsPIC33F SMPS DSC
               		----------------------------------------------

The dsPIC SMPS Family of Digital Signal Controllers feature an on-chip 
Power Supply PWM Module. This module supports several PWM modes typically
used in Power Conversion applications. One mode shown here is the 
True Independent PWM Ouptut Mode.This example is intended to provide a setup guide
for using this mode.

PWM1H and PWM1L have different switching frequencies but the same duty cycle 
(50% of switching period). To verify the independent output feature of the PWM module connect
oscilloscope probes to PWM1H and PWM1L.

Hardware Dependencies: explorer 16 board 

This file provides a brief description of files and folders provided
in the accompanying workspace.

This folder contains the following files:
1. This file
        This file provides a description of the functionality 
	demonstrated by the example source code.

2. True Independent Output.mcw, True Independent Output.mcp
        These are MPLAB� IDE workspace and project files that may be
        used to examine, build, program and debug the example source
        code.

3. True Independent PWM OutpuT.c
        This file is the project source file.

