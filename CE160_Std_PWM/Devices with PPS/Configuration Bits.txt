Boot Segment Write Protect				Boot Segment may be written
Boot Segment Program Flash Code Protection		No Boot Segment
General Code Segment Write Protect			General Code Segment may be written
General Segment Code Protection				No General Segment
Oscillator Source Selection				Internal Fast RC (FRC) 
Internal External Switch Over Mode			Start up with FRC, then switch
Primary Oscillator Source				Primary Oscillator Disabled
OSC2 Pin Function					OSCO pin has digital I/O function
Peripheral Pin Select Configuration			Allow only Re-Configuration
Clock Switching and Monitor				Sw Enabled, Mon Disabled
Watchdog Timer Postscaler				1:32,768
WDT Prescaler						1:128
Watchdog Timer Enable					Disable
POR Timer Value						128ms
Brown-out Reset Enabled					Disabled
Comm Channel Select					Use PGC2/EMUC2 and PGD2/EMUD2
JTAG Port Enable					Disabled
Debugger/Emulation Enable Bit				Reset Into Operational Mode
