                     Readme File for Code Example:
               CE160 - Standard Mode PWM using dsPIC33F SMPS DSC
               ----------------------------------------------

The dsPIC SMPS Family of Digital Signal Controllers feature an on-chip 
Power Supply PWM Module. This module supports several PWM modes typically
used in Power Conversion applications. One of the simplest modes is the 
Standard PWM Mode.

This code example illustrates the use of the Power Supply PWM module in
the Standard PWM mode. This example is intended to provide a setup guide
for using this mode.

PWM1H and PWM2H are configured to be digital outputs, and PWM1L and PWM2L
are selected as PWM outputs. PWM2L is also configured with a phase shift
of 250 nsec from PWM1L.

Hardware Dependencies: 16-Bit 28-Pin Starter Board

This file provides a brief description of files and folders provided
in the accompanying workspace.

This folder contains the following files:
1. This file
        This file provides a description of the functionality 
	demonstrated by the example source code.

2. Standard_PWM.mcw, Standard_PWM.mcp
        These are MPLAB� IDE workspace and project files that may be
        used to examine, build, program and debug the example source
        code.

3. Standard PWM.c
        This file is the project source file.

