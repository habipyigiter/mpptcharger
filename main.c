/*
 * File:   main.c
 * Author: User
 *
 * Created on 22 Ekim 2018 Pazartesi, 10:58
 */

#define FCY 32000000ul

#include "xc.h"
#include <libpic30.h>
#include <stdint.h>
#include <stdbool.h>
#include "core.h"
#include "pwm.h"
#include "st7565p.h"
#include "adc.h"
#include "uart.h"

void Load_Voltage_Control();

void mppt_INCCOND(void);
void mppt_PO(void);
void mppt_PO1(void);
void PandO(void);


#define step_size 50
//#define PWM_freq 160 									//set frequency to 100 kHz
#define TOP_DUTY 500									//set max duty cycle to 80%
#define BOTTOM_DUTY 8000									//set min duty cycle to 20%
#define MAX_CURR 5.0									//set max curr to 5 A
//#define DELAY 1500										// 0.5s * (12000/4)Hz
//#define TIMER_ON 	TA1CTL=TASSEL_1+MC_1+ID_2			// use ACLK, timer on, divide by 4
//#define TIMER_OFF 	TA1CTL=TASSEL_1+MC_0+ID_2			// use ACLK, timer off, divide by 4

// Global Variables

float curr_in = 0; //variable to store current in
float volt_in = 0; //variable to store voltage in
float curr_out = 0; //variable to store current out
float volt_out = 0; //variable to store voltage out

uint16_t duty_cycle = 6100; //variable to store current duty cycle, default to 50%

typedef enum {
    inc, dec
} direction; //enumeration to handle increasing and decreasing state for P&O algorithm
direction dir = inc;
//unsigned char updown = 1; // 0 is down, 1 is up
volatile float vo = 0;
volatile float io = 0;
uint32_t po = 0;

uint16_t Voltage_PV = 0;
uint16_t Voltage_PV_MEAN = 0;
uint16_t Voltage_LOAD = 0;
uint16_t Voltage_LOAD_MEAN = 0;
uint16_t SONT_YUK = 0, SONT_YUK_MEAN = 0;

uint32_t dummy = 0, dummy2 = 0, dummy3 = 0;
uint16_t sayacuart = 0;

uint32_t power = 0, l_power = 0;
uint16_t power16 = 0;
bool updown = 0;
#define MPPT_STEP  3

long ineq = 0, f_iin = 0, f_vin = 0, fl_iin = 0, fl_vin;
double I = 0, V = 0;

int main(void) {

    CORE_InitCore();
    CORE_InitPorts();

    st7565_Init();
    st7565_SetBrightness(20); // CONTRAST
    st7565_NormalDisplay();
    st7565_ClearScreen();
    st7565_DrawString("PV VOLTAGE :", 5, 2, 1, 0);
    st7565_DrawString("AKU VOLTAGE:", 5, 10, 1, 0);
    st7565_DrawString(" YUK  AKIMI:", 5, 18, 1, 0);

    st7565_DrawString("    WATT   :", 5, 48, 1, 0);
    st7565_Refresh();



    LED1 = 1;
    init_PWM();
    init_ADC();
    _ADON = 1; //

    init_UART();

    uint16_t sayac = 0;
    uint16_t sayac2 = 0, sayac4 = 0;

    bool blinkpwm = 0;

    while (1) {

        //         LED2^=1;
        __delay_us(24);

        sayac++;

        Voltage_PV = ADC_RSLT0 * 12;
        Voltage_LOAD = ADC_RSLT1 * 8.11;
        SONT_YUK = ADC_RSLT11 * 3.09;




        sayac2++;
        dummy = dummy + ADC_RSLT1;
        dummy2 = dummy2 + ADC_RSLT0;
        dummy3 = dummy3 + ADC_RSLT11;

        if (sayac2 >= 64) {
            sayac2 = 0;
            Voltage_LOAD_MEAN = dummy / 64 * 8.11;
            Voltage_PV_MEAN = dummy2 / 64 * 12;
            SONT_YUK_MEAN = dummy3 / 64 * 3.09;

            dummy = 0;
            dummy2 = 0;
            dummy3 = 0;

            //            LED3 ^= 1;

            //            Load_Voltage_Control();


            //            sayac4++;
            //            if (sayac4 >= 2) {
            //                sayac4 = 0;
            //                
            //            blinkpwm ^= 1;
            //            if (blinkpwm) {
            //                PDC1 = 5800;
            //            } else {
            //                PDC1 = 5780;
            //            }

            //            sayac4++;
            //            if (sayac4 >= 20) {
            //                sayac4 = 0;
//            LED3 ^= 1;
//            mppt_PO();
            mppt_PO1();
            //            PandO();
            //                PDC1--;
            //                power = Voltage_LOAD_MEAN * SONT_YUK_MEAN;
            //                power16 = power/10;
            //            }


            //            }

            //            blinkpwm ^= 1;
            //            if (blinkpwm) {
            //                PDC1 = 5800;
            //            } else {
            //                PDC1 = 5700;
            //            }
            //            PDC1 = 6000;
            //            mppt_PO();


            //            PDC1=7000;

            if (PDC1 <= 500 || PDC1 >= 8000) {
                PDC1 = 4000;
            }

//
//                        U1TXREG = Divisor16bitHIGH(Voltage_LOAD_MEAN);
//                        while (_TRMT == 0); //0 = Transmit Shift register is not empty, a transmission is in progress or queued
//                        U1TXREG = Divisor16bitLOW(Voltage_LOAD_MEAN);
//                        while (_TRMT == 0); //0 = Transmit Shift register is not empty, a transmission is in progress or queued
//
//                        U1TXREG = Divisor16bitHIGH(SONT_YUK_MEAN);
//                        while (_TRMT == 0); //0 = Transmit Shift register is not empty, a transmission is in progress or queued
//                        U1TXREG = Divisor16bitLOW(SONT_YUK_MEAN);
//                        while (_TRMT == 0); //0 = Transmit Shift register is not empty, a transmission is in progress or queued
//
//                        U1TXREG = Divisor16bitHIGH(Voltage_PV_MEAN);
//                        while (_TRMT == 0); //0 = Transmit Shift register is not empty, a transmission is in progress or queued
//                        U1TXREG = Divisor16bitLOW(Voltage_PV_MEAN);
//                        while (_TRMT == 0); //0 = Transmit Shift register is not empty, a transmission is in progress or queued

            //            sayacuart++;
            //            if (sayacuart >= 16) {
            //                sayacuart = 0;
            //                U1TXREG = 0;
            //                while (_TRMT == 0); //0 = Transmit Shift register is not empty, a transmission is in progress or queued
            //                 U1TXREG = 255;
            //                while (_TRMT == 0); //0 = Transmit Shift register is not empty, a transmission is in progress or queued
            //            }
        }



        if (sayac >= 2500) {
            //            __delay_ms(50);

            sayac = 0;
            //            Voltage_PV = ADC_RSLT0 * 11.9;
            //            Voltage_LOAD = ADC_RSLT1 * 8.08;


            st7565_DrawChar((((Voltage_PV_MEAN) % 10000) / 1000) + 48, 78, 2, 1, 0);
            st7565_DrawChar((((Voltage_PV_MEAN) % 1000) / 100) + 48, 84, 2, 1, 0);
            st7565_DrawChar('.', 90, 2, 1, 0);
            st7565_DrawChar((((Voltage_PV_MEAN) % 100) / 10) + 48, 93, 2, 1, 0);
            st7565_DrawChar((((Voltage_PV_MEAN) % 10) / 1) + 48, 99, 2, 1, 0);


            st7565_DrawChar((((Voltage_LOAD_MEAN) % 10000) / 1000) + 48, 78, 10, 1, 0);
            st7565_DrawChar((((Voltage_LOAD_MEAN) % 1000) / 100) + 48, 84, 10, 1, 0);
            st7565_DrawChar('.', 90, 10, 1, 0);
            st7565_DrawChar((((Voltage_LOAD_MEAN) % 100) / 10) + 48, 93, 10, 1, 0);
            st7565_DrawChar((((Voltage_LOAD_MEAN) % 10) / 1) + 48, 99, 10, 1, 0);



            st7565_DrawChar((((SONT_YUK_MEAN) % 10000) / 1000) + 48, 78, 18, 1, 0);
            st7565_DrawChar((((SONT_YUK_MEAN) % 1000) / 100) + 48, 84, 18, 1, 0);
            st7565_DrawChar('.', 90, 18, 1, 0);
            st7565_DrawChar((((SONT_YUK_MEAN) % 100) / 10) + 48, 93, 18, 1, 0);
            st7565_DrawChar((((SONT_YUK_MEAN) % 10) / 1) + 48, 99, 18, 1, 0);
//
//            st7565_DrawChar((((PDC1) % 10000) / 1000) + 48, 78, 26, 1, 0);
//            st7565_DrawChar((((PDC1) % 1000) / 100) + 48, 84, 26, 1, 0);
//            st7565_DrawChar((((PDC1) % 100) / 10) + 48, 90, 26, 1, 0);
//            st7565_DrawChar((((PDC1) % 10) / 1) + 48, 96, 26, 1, 0);


            st7565_DrawChar((((power16) % 10000) / 1000) + 48, 78, 48, 1, 0);
            st7565_DrawChar((((power16) % 1000) / 100) + 48, 84, 48, 1, 0);
            st7565_DrawChar((((power16) % 100) / 10) + 48, 90, 48, 1, 0);
            st7565_DrawChar((((power16) % 10) / 1) + 48, 96, 48, 1, 0);


            st7565_Refresh();
        }


    }

    return 0;
}

void Load_Voltage_Control() {

    if (Voltage_LOAD_MEAN >= 1360) {
        PDC1 = PDC1 + 1; //azalt
    } else if (Voltage_LOAD_MEAN <= 1358) {
        PDC1 = PDC1 - 1; //artır
    }

    if (PDC1 <= 500 || PDC1 >= 8000) {
        PDC1 = 5120;
    }

}

void mppt_PO(void) {
    power = Voltage_PV_MEAN * SONT_YUK_MEAN;
    power16 = power / 1000;
    if (power < l_power) {
        updown = 1;
    } else {
        updown = 0;
    }

    if (updown) {
        PDC1 += MPPT_STEP;
        //        LED3 = 1;
    } else {
        PDC1 -= MPPT_STEP;
        //        LED3 = 0;
    }


    l_power = power;

}

void mppt_PO1(void) {
    
//    uint16_t dutycle=PDC1;
            
    power = (uint32_t) Voltage_LOAD_MEAN * SONT_YUK_MEAN;
    power16 = power / 10000;
    if (power < l_power) updown ^= 1;
    if (!updown) PDC1 += MPPT_STEP;
    else PDC1 -= MPPT_STEP;
    
    
//    if (Voltage_LOAD_MEAN>=2800) {
//        dutycle += MPPT_STEP;  // gerilimi azalat
//        LED3^=1;
//    }
    
//    PDC1=dutycle;
    
    l_power = power;
}

void PandO(void) {
    uint32_t p = Voltage_PV_MEAN*SONT_YUK_MEAN; // multiply current times voltage to get power, use long since multiplication could overflow an int16
    //volatile float p = curr_out*volt_out;
    switch (dir) // switch based on whether the the duty cycle was last incremented or decremented
    {
        case inc: // if incremented
            if (p > po) // if power has increased
            {
                if (duty_cycle > TOP_DUTY) {
                    duty_cycle -= step_size; // continue incrementing duty cycle
                } else {
                    duty_cycle += step_size; // decrement duty cycle
                    dir = dec;
                }
            } else if (p < po) // if power has decreased
            {
                if (duty_cycle < BOTTOM_DUTY) {
                    duty_cycle += step_size; // decrement duty cycle
                    dir = dec; // change direction to dec
                } else {
                    duty_cycle -= step_size; // if power has increased
                    dir = inc; // change direction to inc
                }

            }
            break;
        case dec: // if decremented
            if (p > po) // if power has increased
            {
                if (duty_cycle < BOTTOM_DUTY) {
                    duty_cycle += step_size; // decrement duty cycle
                } else {
                    duty_cycle -= step_size; // if power has increased
                    dir = inc; // change direction to inc
                }
            } else if (p < po) // if power has decreased
            {
                if (duty_cycle > TOP_DUTY) {
                    duty_cycle -= step_size; // if power has increased
                    dir = inc; // change direction to inc
                } else {
                    duty_cycle += step_size; // decrement duty cycle
                    dir = dec;
                }
            }
            break;
    }
    PDC1 = duty_cycle; //update duty cycle
    po = p; // store power to compare in next iteration
}

