/* 
 * File:   uart.h
 * Author: User
 *
 * Created on 25 Ekim 2018 Perşembe, 15:44
 */

#ifndef UART_H
#define	UART_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#define Divisor16bitHIGH(x)     (uint8_t)(x>>8)
#define Divisor16bitLOW(x)      (uint8_t)(x&0x00FF)

void init_UART();


#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

