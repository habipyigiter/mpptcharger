                     Readme File for Code Example:
               CE020 - Push-Pull Mode PWM using dsPIC SMPS DSC
               ----------------------------------------------

The dsPIC SMPS Family of Digital Signal Controllers feature an on-chip 
Power Supply PWM Module. This module supports several PWM modes typically
used in Power Conversion applications. One commonly used mode of operation
is the Push-Pull PWM mode.

This program illustrates the use of the Push Pull mode of the Power Supply
PWM module. PWM1 and PWM2 are setup to produce 50% duty cycle with a PWM 
period of approximately 500nsec.

The Push-Pull mode produces a pulse on PWMxH on one cycle and PWMxL
on the next PWM cycle. Therefore the PWM duty cycle may appear smaller
than 50% (effectively 25% in this example).

The main application of the push-pull mode is to ensure that no
residual DC charge exists on a transformer winding.

All timing parameters are approximate and assuming 30MIPS operation.

This file provides a brief description of files and folders provided
in the accompanying workspace.

This folder contains the following files:
1. This file
        This file provides a description of the functionality demonstr-
        ated by the example source code.

2. Push-Pull PWM.mcw, Push-Pull PWM.mcp
        These are MPLAB� IDE workspace and project files that may be
        used to examine, build, program and debug the example source
        code.

This folder contains the following sub-folders:

1. src
        This folder contains all the C and Assembler source files (*.c,
        *.s) used in demonstrating the described example.

