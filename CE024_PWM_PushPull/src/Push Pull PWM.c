/******************************************************************************
; * � 2006 Microchip Technology Inc.
; *
; * FileName:        Push Pull PWM.c
; * Dependencies:    Header (.inc) files if applicable, see below
; * Processor:       dsPIC30F2020
; * Compiler:        MPLAB� C30 v3.00 or higher
; * IDE:             MPLAB� IDE v7.52 or later
; * Dev. Board Used: dsPICDEM SMPS Buck Development Board
; *
; * SOFTWARE LICENSE AGREEMENT:
; * Microchip Technology Incorporated ("Microchip") retains all ownership and 
; * intellectual property rights in the code accompanying this message and in all 
; * derivatives hereto.  You may use this code, and any derivatives created by 
; * any person or entity by or on your behalf, exclusively with Microchip,s 
; * proprietary products.  Your acceptance and/or use of this code constitutes 
; * agreement to the terms and conditions of this notice.
; *
; * CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
; * WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
; * TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
; * PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP,S 
; * PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
; *
; * YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
; * IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
; * STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
; * PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
; * ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
; * ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
; * ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
; * THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
; * HAVE THIS CODE DEVELOPED.
; *
; * You agree that you are solely responsible for testing the code and 
; * determining its suitability.  Microchip has no obligation to modify, test, 
; * certify, or support the code.
; *
; * REVISION HISTORY:
; *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; * Author            Date      Comments on this revision
; *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; * Sagar Khare       10/1/06  First release of source file
; *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; *
; * Description:
; *     This program illustrates the use of the Push Pull mode of the SMPS
; * PWM module. PWM1 and PWM2 are setup to produce 50% duty cycle with a
; * PWM period of approximately 500nsec.
; *     The Push-Pull mode produces a pulse on PWMxH on one cycle and PWMxL
; * on the next PWM cycle. Therefore the PWM duty cycle may appear smaller
; * than 50% (effectively 25%). 
; *      The main application of the push-pull mode is to ensure that no
; * residual DC charge exists on a transformer winding.
; * All timing parameters are approximate and assuming 30MIPS operation.
; * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */


#include "p30f2020.h"

/* Configuration Bit Settings */
_FOSCSEL(FRC_PLL)
_FOSC(CSW_FSCM_OFF & FRC_HI_RANGE & OSC2_CLKO)
_FPOR(PWRT_128)
_FGS(CODE_PROT_OFF)
_FBS(BSS_NO_FLASH)


int main()
{
    /* ~~~~~~~~~~~~~~~~~~~~~~~~~ PWM Configuration ~~~~~~~~~~~~~~~~~~~~~~~~~~ */
     
    PTPER = 512;            /* PWM period of approx. 500 nsec
                               PWM Period = PTPER*1.05nsec = 537.6 nsec */
    
    PDC1 = 256;             /* PWM1 pulse width of 250 nsec
                               Duty Cycle = PDC1*1.05nsec = 268.8 nsec */
                               
    PDC2 = 256;             /* PWM2 pulse width of 250 nsec
                               Duty Cycle = PDC2*1.05nsec = 268.8 nsec */
    
    /* Note that a pulse appears only on every other PWM cycle. So in push-pull
       mode, the effective duty cycle is 25% */
    
    DTR1 = 32;              /* 33.6 nsec dead time
                               Dead-time = DTR1*1.05nsec = 33.6 nsec */
                               
    DTR2 = 32;              /* 33.6 nsec dead time
                               Dead-time = DTR2*1.05nsec = 33.6 nsec */
                                       
    ALTDTR1 = 32;           /* 33.6 nsec dead time
                               Alt Dead-time = ALTDTR1*1.05nsec = 33.6 nsec */
                               
    ALTDTR2 = 32;           /* 33.6 nsec dead time
                               Alt Dead-time = ALTDTR2*1.05nsec = 33.6 nsec */
    
    PHASE1 = 0;             /* No phase shift for PWM1 */
    PHASE2 = 128;           /* Phase shift of 128 nsec for PWM2
                               Phase Shift = PHASE2*1.05nsec = 134.4 nsec */
    
    IOCON1bits.PENH = 1;    /* PWM1H output controlled by PWM */
    IOCON1bits.PENL = 1;    /* PWM1L output controlled by PWM */
    IOCON1bits.PMOD = 2;    /* Select Push-Pull PWM mode */
    
    IOCON2bits.PENH = 1;    /* PWM2H output controlled by PWM */
    IOCON2bits.PENL = 1;    /* PWM2L output controlled by PWM */
    IOCON2bits.PMOD = 2;    /* Select Push-Pull PWM mode */
    
    PTCONbits.PTEN = 1;     /* Turn ON PWM module */
    /* ~~~~~~~~~~~~~~~~~~~~~~ End PWM Configuration ~~~~~~~~~~~~~~~~~~~~~~~~~ */
    
    while(1);               /* Infinite loop */
}
