/* 
 * File:   adc.h
 * Author: User
 *
 * Created on 24 Ekim 2018 Çarşamba, 13:53
 */

#ifndef ADC_H
#define	ADC_H

#ifdef	__cplusplus
extern "C" {
#endif
    
   extern int ADC_RSLT0;
   extern int ADC_RSLT1;
   extern int ADC_RSLT11;

void init_ADC(void);


#ifdef	__cplusplus
}
#endif

#endif	/* ADC_H */

