
#define FCY 4000000ull
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <libpic30.h>
#include "fonts.h"
#include "st7565p.h"
#include "core.h"


#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))

#define bit_set(var,b) var |= (1 << b)
#define bit_clear(var,b) var &= ~(1 << b)
#define bit_test(data,bitno) ((data>>bitno)&0x01)

uint8_t buffer[128 * 64 / 8];



/* System 5x8 */
const uint8_t Font5x8[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, /* Space */
    0x00, 0x00, 0x00, 0xfd, 0x00, 0x00, /* ! */ // ?
    0x00, 0x78, 0x85, 0xA5, 0xA5, 0x68, /* " */ // ?
    0x00, 0x3E, 0x41, 0xC1, 0x41, 0x22, /* # */ // Ç
    0x00, 0x24, 0x2a, 0x7f, 0x2a, 0x12, /* 0x */
    0x00, 0x23, 0x13, 0x08, 0x64, 0x62, /* % */
    0x00, 0x36, 0x49, 0x55, 0x22, 0x20, /* & */
    0x00, 0x00, 0x05, 0x03, 0x00, 0x00, /* ' */
    0x00, 0x00, 0x1c, 0x22, 0x41, 0x00, /* ( */
    0x00, 0x00, 0x41, 0x22, 0x1c, 0x00, /* ) */
    0x00, 0x14, 0x08, 0x3e, 0x08, 0x14, /* // */
    0x00, 0x08, 0x08, 0x3e, 0x08, 0x08, /* + */
    0x00, 0x50, 0x30, 0x00, 0x00, 0x00, /* , */
    0x00, 0x08, 0x08, 0x08, 0x08, 0x08, /* - */
    0x00, 0x00, 0x60, 0x60, 0x00, 0x00, /* . */
    0x00, 0x20, 0x10, 0x08, 0x04, 0x02, /* / */
    0x00, 0x3e, 0x51, 0x49, 0x45, 0x3e, /* 0 */
    0x00, 0x00, 0x42, 0x7f, 0x40, 0x00, /* 1 */
    0x00, 0x42, 0x61, 0x51, 0x49, 0x46, /* 2 */
    0x00, 0x21, 0x41, 0x45, 0x4b, 0x31, /* 3 */
    0x00, 0x18, 0x14, 0x12, 0x7f, 0x10, /* 4 */
    0x00, 0x27, 0x45, 0x45, 0x45, 0x39, /* 5 */
    0x00, 0x3c, 0x4a, 0x49, 0x49, 0x30, /* 6 */
    0x00, 0x01, 0x71, 0x09, 0x05, 0x03, /* 7 */
    0x00, 0x36, 0x49, 0x49, 0x49, 0x36, /* 8 */
    0x00, 0x06, 0x49, 0x49, 0x29, 0x1e, /* 9 */
    0x00, 0x00, 0x36, 0x36, 0x00, 0x00, /* : */
    0x00, 0x00, 0x56, 0x36, 0x00, 0x00, /* ; */
    0x00, 0x08, 0x14, 0x22, 0x41, 0x00, /* < */
    0x00, 0x14, 0x14, 0x14, 0x14, 0x00, /* = */ //  0x14, 0x14, 0x14, 0x14, 0x14, /* = */
    0x00, 0x00, 0x41, 0x22, 0x14, 0x08, /* > */
    0x00, 0x02, 0x01, 0x51, 0x09, 0x06, /* ? */
    0x00, 0x3e, 0x41, 0x5d, 0x55, 0x1e, /* @ */
    0x00, 0x7e, 0x11, 0x11, 0x11, 0x7e, /* A */
    0x00, 0x7f, 0x49, 0x49, 0x49, 0x36, /* B */
    0x00, 0x3e, 0x41, 0x41, 0x41, 0x22, /* C */
    0x00, 0x7f, 0x41, 0x41, 0x22, 0x1c, /* D */
    0x00, 0x7f, 0x49, 0x49, 0x49, 0x41, /* E */
    0x00, 0x7f, 0x09, 0x09, 0x09, 0x01, /* F */
    0x00, 0x3e, 0x41, 0x49, 0x49, 0x7a, /* G */
    0x00, 0x7f, 0x08, 0x08, 0x08, 0x7f, /* H */
    0x00, 0x00, 0x41, 0x7f, 0x41, 0x00, /* I */
    0x00, 0x20, 0x40, 0x41, 0x3f, 0x01, /* J */
    0x00, 0x7f, 0x08, 0x14, 0x22, 0x41, /* K */
    0x00, 0x7f, 0x40, 0x40, 0x40, 0x40, /* L */
    0x00, 0x7f, 0x02, 0x0c, 0x02, 0x7f, /* M */
    0x00, 0x7f, 0x04, 0x08, 0x10, 0x7f, /* N */
    0x00, 0x3e, 0x41, 0x41, 0x41, 0x3e, /* O */
    0x00, 0x7f, 0x09, 0x09, 0x09, 0x06, /* P */
    0x00, 0x3e, 0x41, 0x51, 0x21, 0x5e, /* Q */
    0x00, 0x7f, 0x09, 0x19, 0x29, 0x46, /* R */
    0x00, 0x26, 0x49, 0x49, 0x49, 0x32, /* S */
    0x00, 0x01, 0x01, 0x7f, 0x01, 0x01, /* T */
    0x00, 0x3f, 0x40, 0x40, 0x40, 0x3f, /* U */
    0x00, 0x1f, 0x20, 0x40, 0x20, 0x1f, /* V */
    0x00, 0x3f, 0x40, 0x38, 0x40, 0x3f, /* W */
    0x00, 0x63, 0x14, 0x08, 0x14, 0x63, /* X */
    0x00, 0x07, 0x08, 0x70, 0x08, 0x07, /* Y */
    0x00, 0x61, 0x51, 0x49, 0x45, 0x43, /* Z */
    0x00, 0x78, 0x85, 0x84, 0x85, 0x78, /* [ */ // Ö
    0x00, 0x02, 0x04, 0x08, 0x10, 0x20, /* \ */
    0x00, 0x26, 0x49, 0xC9, 0x49, 0x32, /* ] */ // ?
    0x00, 0x7C, 0x81, 0x80, 0x81, 0x7C, /* ^ */ // Ü
    0x00, 0x40, 0x40, 0x40, 0x40, 0x40, /* _ */
    0x00, 0x00, 0x00, 0x03, 0x05, 0x00, /* ` */
    0x00, 0x20, 0x54, 0x54, 0x54, 0x78, /* a */
    0x00, 0x7F, 0x44, 0x44, 0x44, 0x38, /* b */
    0x00, 0x38, 0x44, 0x44, 0x44, 0x44, /* c */
    0x00, 0x38, 0x44, 0x44, 0x44, 0x7f, /* d */
    0x00, 0x38, 0x54, 0x54, 0x54, 0x18, /* e */
    0x00, 0x04, 0x04, 0x7e, 0x05, 0x05, /* f */
    0x00, 0x08, 0x54, 0x54, 0x54, 0x3c, /* g */
    0x00, 0x7f, 0x08, 0x04, 0x04, 0x78, /* h */
    0x00, 0x00, 0x44, 0x7d, 0x40, 0x00, /* i */
    0x00, 0x20, 0x40, 0x44, 0x3d, 0x00, /* j */
    0x00, 0x7f, 0x10, 0x28, 0x44, 0x00, /* k */
    0x00, 0x00, 0x41, 0x7f, 0x40, 0x00, /* l */
    0x00, 0x7c, 0x04, 0x7c, 0x04, 0x78, /* m */
    0x00, 0x7c, 0x08, 0x04, 0x04, 0x78, /* n */
    0x00, 0x38, 0x44, 0x44, 0x44, 0x38, /* o */
    0x00, 0x7c, 0x14, 0x14, 0x14, 0x08, /* p */
    0x00, 0x08, 0x14, 0x14, 0x14, 0x7c, /* q */
    0x00, 0x7c, 0x08, 0x04, 0x04, 0x00, /* r */
    0x00, 0x48, 0x54, 0x54, 0x54, 0x24, /* s */
    0x00, 0x04, 0x04, 0x3f, 0x44, 0x44, /* t */
    0x00, 0x3c, 0x40, 0x40, 0x20, 0x7c, /* u */
    0x00, 0x1c, 0x20, 0x40, 0x20, 0x1c, /* v */
    0x00, 0x3c, 0x40, 0x30, 0x40, 0x3c, /* w */
    0x00, 0x44, 0x28, 0x10, 0x28, 0x44, /* x */
    0x00, 0x0c, 0x50, 0x50, 0x50, 0x3c, /* y */
    0x00, 0x44, 0x64, 0x54, 0x4c, 0x44, /* z */
    0x00, 0x08, 0x36, 0x41, 0x41, 0x00, /* { */
    0x00, 0x00, 0x00, 0x77, 0x00, 0x00, /* | */
    0x00, 0x00, 0x41, 0x41, 0x36, 0x08, /* } */
    0x00, 0x08, 0x08, 0x2a, 0x1c, 0x08, /* <- */
    0x00, 0x08, 0x1c, 0x2a, 0x08, 0x08, /* -> */
    0x00, 0xff, 0xff, 0xff, 0xff, 0xff, /*  */
    0x00, 0xA0, 0xA0, 0xA0, 0xA0, 0xA0, // Custom =
    0x00, 0x00, 0x06, 0x09, 0x09, 0x06, // Custom derece
    0x7c, 0x86, 0xb5, 0xb5, 0x86, 0x7c, // Custom kilit
    0xfe, 0x82, 0x38, 0x10, 0x82, 0xD6, // fan1
    0x82, 0x10, 0x38, 0x82, 0xFE, 0x00, // fan2
    0x18, 0x18, 0x24, 0x42, 0xFF, 0x00, // buzzer
    0x28, 0x10, 0x28, 0x00, 0x00, 0x00 // buzzer
};

void st7565_Command(uint8_t command) //8080 interface
{
    //
    //    /* 6800 */
    //
    //    A0 = 0;
    //    Nop();
    //    DATA = cc;
    //    E = 1;
    //    E = 0;
    //    /* */
    //    command = Mirror(command);
    A0 = 0;
    CS1 = 0;
    //    RD = 1;
    //  RW = 0;
    int8_t i;
    for (i = 7; i >= 0; i--) {
        SPI_SCL = 0;
        //        __delay_us(10);
        if (command & (1 << (i)))
            SPI_SI = 1;
        else
            SPI_SI = 0;
        //      delayMicroseconds(5);      //need to slow down the data rate for Due and Zero
        SPI_SCL = 1;
    }

    //    DATA = command;
    //   RW = 1;
    //  CS1 = 1;

}

void st7565_Data(uint8_t data) //8080 interface
{
    //    /* 6800 */
    //    A0 = 1;
    //    Nop();
    //    DATA = dd;
    //    E = 1;
    //    E = 0;
    //    /* */

    //    data = Mirror(data);
    A0 = 1;
    CS1 = 0;
    //    RD = 1;
    // RW = 0;
    int8_t i;
    for (i = 7; i >= 0; i--) {
        SPI_SCL = 0;
        //        __delay_us(1);
        if (data & (1 << (i)))
            SPI_SI = 1;
        else
            SPI_SI = 0;
        //        __delay_us(1);
        SPI_SCL = 1;
    }
    //  DATA = data;
    //  RW = 1;
    //  CS1 = 1;


}

void writeBuffer(uint8_t *buffer) {
    uint8_t c, p;
    bool refresh = 0;
    uint16_t i = 0;



        st7565_Command(0xb0); //Page address set
        st7565_Command(0x40); //Display start line  set
        st7565_Command(0x10); //column address set :upper
        st7565_Command(0x00); //column address set :lower

        for (p = 0; p < 8; p++) {

            st7565_Command(0xb0 + p); //Page address set
            st7565_Command(0x40); //Display start line  set
            st7565_Command(0x10); //column address set :upper
            st7565_Command(0x00); //column address set :lower

            for (c = 0; c < 128; c++) {
                st7565_Data(buffer[(128 * p) + c]);
            }
        }

        i = 0;


 



}

void st7565_Init(void) {

    //RW=1;


    uint16_t i = 0;
    /* buffer i 0 a esitle*/
    for (i = 0; i < 1024; i++) {
        buffer[i] = 0;
    }

//    pre_buffer[20] = 8; // bufferin kopyası baslangicta aynı degerler olasın diye herhangi bir deger degistirildi.

    RES = 0;
    __delay_ms(50);
    RES = 1;
    __delay_ms(50);
    // Configure Display
    st7565_Command(ST7565_CMD_DISPLAY_OFF); // 0XAE
    st7565_Command(ST7565_CMD_SET_BIAS_7); // LCD Bias Select
    st7565_Command(ST7565_CMD_SET_ADC_REVERSE); // ADC Select
    st7565_Command(ST7565_CMD_SET_COM_REVERSE); // SHL Select
    st7565_Command(ST7565_CMD_SET_RESISTOR_RATIO | 0x04); // internal resister select
    st7565_Command(ST7565_CMD_SET_V0_VOLTAGE_REGULATOR);
    st7565_Command(0x20); // start line 0

    st7565_Command(ST7565_CMD_SET_POWER_CONTROL | 0x04); // Turn on voltage converter (VC=1, VR=0, VF=0)
    __delay_ms(50); // Wait 50mS
    st7565_Command(ST7565_CMD_SET_POWER_CONTROL | 0x06); // Turn on voltage regulator (VC=1, VR=1, VF=0)
    __delay_ms(50); // Wait 50mS
    st7565_Command(ST7565_CMD_SET_POWER_CONTROL | 0x07); // Turn on voltage follower
    __delay_ms(50); // Wait 10mS

    st7565_Command(ST7565_CMD_SET_DISP_REVERSE); // //Display normal/reverse  : normal 0xA6
    st7565_Command(ST7565_CMD_SET_ALLPTS_NORMAL); // //Display all point :normal 0xA4
    st7565_Command(ST7565_CMD_SET_BOOSTER_4x); // //Buydisplay Ciziminde 4x boost devresi cizilmis

    st7565_SetBrightness(20); // CONTRAST

    st7565_Command(ST7565_CMD_SET_DISP_START_LINE); // Initial Display Line 0x40
    st7565_Command(ST7565_CMD_SET_PAGE); // Page address set 0xB0
    st7565_Command(ST7565_CMD_SET_COLUMN_UPPER); // column address set :upper 0x10
    st7565_Command(ST7565_CMD_SET_COLUMN_LOWER); // column address set :lower 0x00

    st7565_Command(ST7565_CMD_DISPLAY_ON); // 0XAF

}

void st7565_SetBrightness(uint8_t val) {
    st7565_Command(ST7565_CMD_SET_VOLUME_FIRST);
    st7565_Command(ST7565_CMD_SET_VOLUME_SECOND | (val & 0x3f));
}

void st7565_ClearScreen(void) {
    uint16_t i = 0;

    for (i = 0; i < 1024; i++) {
        buffer[i] = 0;
    }
    st7565_Refresh();
}

void st7565_Refresh(void) {
    writeBuffer(buffer);
}

void st7565_DrawPixel(uint8_t x, uint8_t y, bool color) {
    if ((x >= 128) || (y >= 64))
        return;
    if (color) {
        buffer[(128 * (y / 8)) + x] |= (1 << (y % 8));
    } else {
        buffer[(128 * (y / 8)) + x] &= ~(1 << (y % 8));
    }
}

//void st7565_DrawCircle(unsigned char cx, unsigned char cy, unsigned char radius, bool color) {
//    int x, y, xchange, ychange, radiusError;
//    x = radius;
//    y = 0;
//    xchange = 1 - 2 * radius;
//    ychange = 1;
//    radiusError = 0;
//    while (x >= y) {
//        st7565_DrawPixel(cx + x, cy + y, color);
//        st7565_DrawPixel(cx - x, cy + y, color);
//        st7565_DrawPixel(cx - x, cy - y, color);
//        st7565_DrawPixel(cx + x, cy - y, color);
//        st7565_DrawPixel(cx + y, cy + x, color);
//        st7565_DrawPixel(cx - y, cy + x, color);
//        st7565_DrawPixel(cx - y, cy - x, color);
//        st7565_DrawPixel(cx + y, cy - x, color);
//        y++;
//        radiusError += ychange;
//        ychange += 2;
//        if (2 * radiusError + xchange > 0) {
//            x--;
//            radiusError += xchange;
//            xchange += 2;
//        }
//    }
//}

void st7565_DrawLine(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, bool color) {
    uint8_t cnt;

    uint8_t dy, dx, x, y;
    int8_t addx, addy;
    int16_t err;

    if (x1 == x2 || y1 == y2) {
        if (x1 == x2) {
            for (cnt = min(y1, y2); cnt <= max(y1, y2); cnt++)
                st7565_DrawPixel(x1, cnt, color);
        } else {
            for (cnt = min(x1, x2); cnt <= max(x1, x2); cnt++)
                st7565_DrawPixel(cnt, y1, color);
        }
    } else {
        dx = max(x1, x2) - min(x1, x2);
        dy = max(y1, y2) - min(y1, y2);

        x = x1;
        y = y1;

        if (x1 > x2)
            addx = -1;
        else
            addx = 1;

        if (y1 > y2)
            addy = -1;
        else
            addy = 1;

        if (dx >= dy) {
            err = 2 * dy - dx;

            for (cnt = 0; cnt <= dx; ++cnt) {
                st7565_DrawPixel(x, y, color);

                if (err < 0) {
                    err += 2 * dy;
                    x += addx;
                } else {
                    err += 2 * dy - 2 * dx;
                    x += addx;
                    y += addy;
                }
            }
        } else {
            err = 2 * dx - dy;

            for (cnt = 0; cnt <= dy; ++cnt) {
                st7565_DrawPixel(x, y, color);

                if (err < 0) {
                    err += 2 * dx;
                    y += addy;
                } else {
                    err += 2 * dx - 2 * dy;
                    x += addx;
                    y += addy;
                }
            }
        }
    }
}

void st7565_DrawRectangle(int8_t x1, int8_t y1, int8_t x2, int8_t y2, bool color) {
    st7565_DrawLine(x1, y1, x2, y1, color); // Draw the 4 sides
    st7565_DrawLine(x1, y2, x2, y2, color);
    st7565_DrawLine(x1, y1, x1, y2, color);
    st7565_DrawLine(x2, y1, x2, y2, color);
}

void st7565_DrawRectangleFill(int8_t x1, int8_t y1, int8_t x2, int8_t y2, bool color) {
    int8_t i, xmin, xmax, ymin, ymax;

    if (x1 < x2) //  Find x min and max
    {
        xmin = x1;
        xmax = x2;
    } else {
        xmin = x2;
        xmax = x1;
    }

    if (y1 < y2) // Find the y min and max
    {
        ymin = y1;
        ymax = y2;
    } else {
        ymin = y2;
        ymax = y1;
    }

    for (; xmin <= xmax; ++xmin) {
        for (i = ymin; i <= ymax; ++i) {
            st7565_DrawPixel(xmin, i, color);
        }
    }

}

//void GLCD_Rectangle(unsigned char x, unsigned char y, unsigned char b, unsigned char a, bool color) {
//    unsigned char j; // zmienna pomocnicza
//    // rysowanie linii pionowych (boki)
//    for (j = 0; j < a; j++) {
//        st7565_DrawPixel(x, y + j, color);
//        st7565_DrawPixel(x + b - 1, y + j, color);
//    }
//    // rysowanie linii poziomych (podstawy)
//    for (j = 0; j < b; j++) {
//        st7565_DrawPixel(x + j, y, color);
//        st7565_DrawPixel(x + j, y + a - 1, color);
//    }
//}

//void glcd_bar(int x1, int y1, int x2, int y2, int width, bool color) {
//    signed int x, y, addx, addy, j;
//    signed long P, dx, dy, c1, c2;
//    int i;
//    dx = abs((signed int) (x2 - x1));
//    dy = abs((signed int) (y2 - y1));
//    x = x1;
//    y = y1;
//    c1 = -dx * x1 - dy*y1;
//    c2 = -dx * x2 - dy*y2;
//
//    if (x1 > x2) {
//        addx = -1;
//        c1 = -dx * x2 - dy*y2;
//        c2 = -dx * x1 - dy*y1;
//    } else
//        addx = 1;
//    if (y1 > y2) {
//        addy = -1;
//        c1 = -dx * x2 - dy*y2;
//        c2 = -dx * x1 - dy*y1;
//    } else
//        addy = 1;
//
//    if (dx >= dy) {
//        P = 2 * dy - dx;
//
//        for (i = 0; i <= dx; ++i) {
//            for (j = -(width / 2); j < width / 2 + width % 2; ++j) {
//                if (dx * x + dy * (y + j) + c1 >= 0 && dx * x + dy * (y + j) + c2 <= 0)
//                    st7565_DrawPixel(x, y + j, color);
//            }
//            if (P < 0) {
//                P += 2 * dy;
//                x += addx;
//            } else {
//                P += 2 * dy - 2 * dx;
//                x += addx;
//                y += addy;
//            }
//        }
//    } else {
//        P = 2 * dx - dy;
//
//        for (i = 0; i <= dy; ++i) {
//            if (P < 0) {
//                P += 2 * dx;
//                y += addy;
//            } else {
//                P += 2 * dx - 2 * dy;
//                x += addx;
//                y += addy;
//            }
//            for (j = -(width / 2); j < width / 2 + width % 2; ++j) {
//                if (dx * x + dy * (y + j) + c1 >= 0 && dx * x + dy * (y + j) + c2 <= 0)
//                    st7565_DrawPixel(x + j, y, color);
//            }
//        }
//    }
//}

/* y degeri seklinde ilerler 0-7 aras? 1.sat?r 7-15 2. sat?r gibi toplam 8 sat?r */
void st7565_DrawChar(uint8_t letter, uint8_t x, uint8_t y, uint8_t size, bool color) {
    if ((x >= 128) || (y >= 64))
        return;
    Nop();
    if (size == 0) {
        buffer[(128 * (y / 8)) + x] = Font3x6[((letter - 32)*3)];
        buffer[(128 * (y / 8)) + ++x] = Font3x6[((letter - 32)*3) + 1];
        buffer[(128 * (y / 8)) + ++x] = Font3x6[((letter - 32)*3) + 2];
    } else if (size == 1) {
        uint8_t i = 0;
        for (i = 0; i < 6; i++) {
            if (color == 0) {
                buffer[(128 * ((y / 8))) + x++] = Font5x8[((letter - 32)*6) + i];
            } else {
                buffer[(128 * ((y / 8))) + x++] = ~Font5x8[((letter - 32)*6) + i];
            }
        }
        //        buffer[(128 * (y / 8)) + x] = Font5x8[((letter - 32)*6)];
        //        buffer[(128 * (y / 8)) + ++x] = Font5x8[((letter - 32)*6) + 1];
        //        buffer[(128 * (y / 8)) + ++x] = Font5x8[((letter - 32)*6) + 2];
        //        buffer[(128 * (y / 8)) + ++x] = Font5x8[((letter - 32)*6) + 3];
        //        buffer[(128 * (y / 8)) + ++x] = Font5x8[((letter - 32)*6) + 4];
        //        buffer[(128 * (y / 8)) + ++x] = Font5x8[((letter - 32)*6) + 5];

    } else if (size == 2) {
        uint8_t i = 0;
        for (i = 0; i < 12; i++) {

         
            x++;
        }
    }

}

void st7565_DrawChar2(uint8_t letter, uint8_t x, uint8_t y, uint8_t size, bool color) {
    if ((x >= 128) || (y >= 64))
        return;
    Nop();
    uint8_t i = 0, k = 0;

    switch (size) {
        case 0:

            for (i = 0; i < 3; i++) {
                if (color == 0) {

                    for (k = 0; k < 7; ++k) // Loop through the vertical pixels
                    {
                        if (bit_test(Font3x6[((letter - 32)*3) + i], k)) {
                            st7565_DrawPixel(x, y + k, 1);
                        } else {
                            st7565_DrawPixel(x, y + k, 0);
                        }
                    }
                    x++;
                    //                    buffer[(64 * ((y / 8) + cs2)) + x++] = Font5x8[((letter - 32)*6) + i];
                } else {

                    for (k = 0; k < 7; ++k) // Loop through the vertical pixels
                    {
                        if (bit_test(Font3x6[((letter - 32)*3) + i], k)) {

                            st7565_DrawPixel(x, y + k, 0);
                        } else {
                            st7565_DrawPixel(x, y + k, 1);
                        }

                    }
                    x++;
                    //                    buffer[(64 * ((y / 8) + cs2)) + x++] = ~Font5x8[((letter - 32)*6) + i];
                }
            }

            //            for (i = 0; i < 3; i++) {
            //                buffer[(128 * ((y / 8))) + x++] = Font3x6[((letter - 32)*3) + i];
            //            }
            break;
        case 1:
            if (color == 1) {
                st7565_DrawLine(x, y - 1, x + 5, y - 1, 1);
                st7565_DrawLine(x, y + 7, x + 5, y + 7, 1);
                st7565_DrawLine(x + 6, y - 1, x + 6, y + 7, 1);
            } else {
                st7565_DrawLine(x, y - 1, x + 5, y - 1, 0);
                st7565_DrawLine(x, y + 7, x + 5, y + 7, 0);
                st7565_DrawLine(x + 6, y - 1, x + 6, y + 7, 0);
            }

            for (i = 0; i < 6; i++) {

                if (color == 0) {

                    for (k = 0; k < 7; ++k) // Loop through the vertical pixels
                    {
                        if (bit_test(Font5x8[((letter - 32)*6) + i], k)) {
                            st7565_DrawPixel(x, y + k, 1);
                        } else {
                            st7565_DrawPixel(x, y + k, 0);
                        }
                    }
                    x++;
                    //                    buffer[(64 * ((y / 8) + cs2)) + x++] = Font5x8[((letter - 32)*6) + i];
                } else {

                    for (k = 0; k < 7; ++k) // Loop through the vertical pixels
                    {
                        if (bit_test(Font5x8[((letter - 32)*6) + i], k)) {

                            st7565_DrawPixel(x, y + k, 0);
                        } else {
                            st7565_DrawPixel(x, y + k, 1);
                        }

                    }
                    x++;
                    //                    buffer[(64 * ((y / 8) + cs2)) + x++] = ~Font5x8[((letter - 32)*6) + i];
                }
            }
            break;
        case 2:

            for (i = 0; i < 12; i++) {

                x++;
            }
            break;
        default:
            for (i = 0; i < 5; i++) {

                buffer[(128 * ((y / 8))) + x++] = Font5x8[((letter - 32)*5) + i];
            }
            break;
    }
}

//void st7565_DrawCharReverse(char letter, uint8_t x, uint8_t y, uint8_t size) {
//    if ((x >= 128) || (y >= 64))
//        return;
//    Nop();
//    if (size == 0) {
//        buffer[(128 * (y / 8)) + x] = ~Font3x6[((letter - 32)*3)];
//        buffer[(128 * (y / 8)) + ++x] = ~Font3x6[((letter - 32)*3) + 1];
//        buffer[(128 * (y / 8)) + ++x] = ~Font3x6[((letter - 32)*3) + 2];
//    } else {
//        buffer[(128 * (y / 8)) + x] = ~Font5x8[((letter - 32)*5)];
//        buffer[(128 * (y / 8)) + ++x] = ~Font5x8[((letter - 32)*5) + 1];
//        buffer[(128 * (y / 8)) + ++x] = ~Font5x8[((letter - 32)*5) + 2];
//        buffer[(128 * (y / 8)) + ++x] = ~Font5x8[((letter - 32)*5) + 3];
//        buffer[(128 * (y / 8)) + ++x] = ~Font5x8[((letter - 32)*5) + 4];
//
//    }
//}

void st7565_DrawString(char *str, uint8_t x, uint8_t y, uint8_t size, bool color) {
    while (*str) {
        st7565_DrawChar(*str, x, y, size, color);
        str++;
        if (size == 0) {
            x = x + 4;
        } else {
            x = x + 6;
        }
    }
};

void st7565_DrawString2(char *str, uint8_t x, uint8_t y, uint8_t size, bool color) {
    while (*str) {
        st7565_DrawChar2(*str, x, y, size, color);
        str++;
        if (size == 0) {
            x = x + 4;
        } else if (size == 1) {
            x = x + 6;
        } else if (size == 2) {
            x = x + 11;
        }
    }
};

//void st7565_DrawStringReverse(char *str, uint8_t x, uint8_t y, uint8_t size) {
//    while (*str) {
//        st7565_DrawCharReverse(*str, x, y, size);
//        str++;
//        if (size == 0) {
//            x = x + 4;
//        } else {
//            x = x + 6;
//        }
//    }
//};

void st7565_ReverseDisplay() {
    st7565_Command(ST7565_CMD_SET_DISP_REVERSE); // //Display normal/reverse  : normal 0xA6
};

void st7565_NormalDisplay() {
    st7565_Command(ST7565_CMD_SET_DISP_NORMAL); // //Display normal/reverse  : normal 0xA6
};

uint8_t Mirror(uint8_t value) {
    uint8_t v; // input bits to be reversed
    v = value;
    uint8_t r = v; // r will be reversed bits of v; first get LSB of v
    uint8_t s = sizeof (v) * 7; // extra shift needed at end

    for (v >>= 1; v; v >>= 1) {
        r <<= 1;
        r |= v & 1;
        s--;
    }
    return r <<= s; // shift when v's highest bits are zero
}

void foo() {

}

void st7565_12X16_Draw(uint8_t letter, uint8_t x, uint8_t y,uint8_t color) {
    uint8_t i = 0, j = 0;
    uint8_t data = 0, m = 0;
    
    for (j = 0; j < 12; j++) {
        
     
        
        for (i = 0; i < 8; i++) {
            m = data;
            m = m & 0x01;
            if (color) {
                
            }else {
                m=!m;
            }            
            if (m) {
                st7565_DrawPixel(x+j, y + i, 1);
            } else {
                st7565_DrawPixel(x+j, y + i, 0);
            }
            data = data >> 1;
        }


        
        for (i = 0; i < 8; i++) {
            m = data;
            m = m & 0x01;
             if (color) {
                
            }else {
                m=!m;
            }   
            if (m) {
                st7565_DrawPixel(x+j, y + i+8, 1);
            } else {
                st7565_DrawPixel(x+j, y + i+8, 0);
            }
            data = data >> 1;
        }
        
    }
}
