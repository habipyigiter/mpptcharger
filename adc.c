#include <xc.h>
#include <stdbool.h>
#include "adc.h"
#include "core.h"
#include "uart.h"

int ADC_RSLT0 = 0;
int ADC_RSLT1 = 0;
int ADC_RSLT11 = 0;
bool blink = 0;

//void __attribute__((__interrupt__, no_auto_psv)) _ADCP0Interrupt() 

void __attribute__((__interrupt__, no_auto_psv)) _ADCP0Interrupt() {
    IFS6bits.ADCP0IF = 0;
    ADC_RSLT0 = ADCBUF0; // Read AN0 conversion result
    ADC_RSLT1 = ADCBUF1; // Read AN1 conversion result 
    ADSTATbits.P0RDY = 0; // Clear the data is ready in buffer bits   
//        LED3 ^= 1;
//    LED3 = 0;
}

void __attribute__((__interrupt__, no_auto_psv)) _ADCP5Interrupt() {
    _ADCP5IF = 0;
    ADC_RSLT11 = ADCBUF11;
    ADSTATbits.P5RDY = 0; // Clear the data is ready in buffer bits  
//    LED3 ^= 1;
//    LED3=1;
}

void init_ADC(void) {


    ADCONbits.FORM = 0; //Integer data format
    ADCONbits.EIE = 0; //Early Interrupt disabled
    ADCONbits.ORDER = 0; //Convert even channel first
    ADCONbits.SEQSAMP = 0; //Select simultaneous sampling
    
    ADCONbits.ASYNCSAMP = 1; // Asynchronous sampling
    
    ADCONbits.ADCS = 7; //ADC clock = FADC/8 = 120MHz / 6 = 20MHz 011 = FADC/4 (default)

    ADPCFGbits.PCFG0 = 0; //select CH0 as analog pin 
    ADPCFGbits.PCFG1 = 0; //select CH1 as analog pin
    ADPCFGbits.PCFG11 = 0; //select CH10 as analog pin

    IFS6bits.ADCP0IF = 0; //Clear ADC Pair 0 interrupt flag 
    IPC27bits.ADCP0IP = 5; //Set ADC Pair 0 interrupt priority
    IEC6bits.ADCP0IE = 1; //Enable the ADC Pair 0 interrupt

    ADSTATbits.P0RDY = 0; //Clear Pair 0 data ready bit
    ADCPC0bits.IRQEN0 = 1; //Enable ADC Interrupt pair 0 
    ADCPC0bits.TRGSRC0 = 4; //ADC Pair 0 triggered by PWM1 Trigger

    _ADCP5IF = 0;
    _ADCP5IP = 6;
    _ADCP5IE = 1;

    _P5RDY=0;
    _IRQEN5 = 1;
    _TRGSRC5 = 4;

    //    TRGCON1bits.DTM = 1; //dual trigger mode
    TRGCON1bits.DTM = 0;
    TRIG1bits.TRGCMP = 0; //Primary trig compare value 
    STRIG1bits.STRGCMP = 0xFFF; //secondary trig compare value

    //    TRGCON1bits.TRGDIV = 0; // Trigger generated every PWM cycle
    TRGCON1bits.TRGDIV = 2;     // Trigger output for every 3rd trigger event

    TRGCON1bits.TRGSTRT = 0; // enable Trigger generated after 0 PWM cycles 
    TRIG1 = 1; // Trigger compare value



}
