/**********************************************************************
* ) 2007 Microchip Technology Inc.
*
; * FileName:        DualTrigADC.c
; * Dependencies:    Header (.inc) files if applicable, see below
; * Processor:       dsPIC33FJ64GS610
; * Compiler:        MPLAB. C30 v3.01 or higher
; * IDE:             MPLAB. IDE v8.36.02 or later
; * Demo Board:      explorer 16 board
; *
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip's
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP'S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
 * REVISION HISTORY:
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Author 		Date		Comments on this revision
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Jin Wang      02/20/09  		First release of source file
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Code Description
*  In this code example, ADC is triggered twice in every PWM cycle (~4.4mS). the 1st trigger is at the start of PWM cycle, while the 2nd at middle
*  For simplicity, only ADC input pair 0 (AN0, AN1) is sampled and converted. 
*  For validation purpose, every ADC pair conversion will trig an interrupt. In the interrupt service routine, 
*  ADC result is read and PB4 pin is toggled to indicate an ADC pair conversion. As the result, on oscilloscope, PB4 and PWM1H keep same frequency
 **********************************************************************/
#include "p33FJ64GS610.h"

_FOSCSEL(FNOSC_FRC)
_FOSC(FCKSM_CSECMD & OSCIOFNC_ON)
_FWDT(FWDTEN_OFF)
_FPOR(FPWRT_PWR128 )
_FICD(ICS_PGD1 & JTAGEN_OFF)

int 		ADC_RSLT0=0;
int 		ADC_RSLT1=0;
void 	init_PWM(void);
void 	init_ADC(void);

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */											  
int main(void)
{
	// Configure PLL prescaler, PLL postscaler, PLL divisor  
	PLLFBD=41; 							// M = PLLFBD + 2  
	CLKDIVbits.PLLPOST=0;   				// N1 = 2  
	CLKDIVbits.PLLPRE=0;    				// N2 = 2  

    	__builtin_write_OSCCONH(0x01);			// New Oscillator FRC w/ PLL  
    	__builtin_write_OSCCONL(0x01);  		// Enable Switch  
      
	while(OSCCONbits.COSC != 0b001);		// Wait for new Oscillator to become FRC w/ PLL    
    	while(OSCCONbits.LOCK != 1);			// Wait for Pll to Lock  

	ACLKCONbits.FRCSEL = 1;				// FRC provides input for Auxiliary PLL (x16)  
	ACLKCONbits.SELACLK = 1;			// Auxiliary Ocillator provides clock source for PWM & ADC  
	ACLKCONbits.APSTSCLR = 7;			// Divide Auxiliary clock by 1  
	ACLKCONbits.ENAPLL = 1;				// Enable Auxiliary PLL  
	
	while(ACLKCONbits.APLLCK != 1);		// Wait for Auxiliary PLL to Lock  

	init_PWM();								// PWM Setup		
	init_ADC();								// ADC Setup 
	
	TRISBbits.TRISB4  = 0;		    	 		//RB4 as output

    	PTCONbits.PTEN = 1;					// Enable the PWM  
	ADCONbits.ADON = 1;					// Enable the ADC  

    	while(1);
} 

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
void __attribute__((__interrupt__,no_auto_psv)) _ADCP0Interrupt ()
 {	IFS6bits.ADCP0IF=0;
	ADC_RSLT0 = ADCBUF0; 			// Read AN0 conversion result
	ADC_RSLT1 = ADCBUF1; 			// Read AN1 conversion result 
	ADSTATbits.P0RDY = 0; 				// Clear the data is ready in buffer bits
	LATBbits.LATB4^=1;				// Toggle B4 pin output
}
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
void init_PWM(void)    
{
 	PTCON2bits.PCLKDIV=0b110;		//PWM input clk devider =64
	PHASE1 = 0xFFFF;					// Timer1 period=max, this is just convenient for validation 
	PDC1 = 0x7FFF; 					// Duty Cycle is 50%  
	IOCON1bits.PENH = 1;                  	// PWM1H is controlled by PWM module
	IOCON1bits.PENL = 1;                  	// PWM1H is controlled by PWM module	
	IOCON1bits.POLH = 0;                  	// Drive signals are active-high 
	IOCON1bits.PMOD=1;				// PWM pair works in Independent Mode
	PWMCON1bits.ITB = 1;				// Select Independent Timebase mode
}
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
void init_ADC(void)  
{	 
    	ADCONbits.FORM = 0;			//Integer data format
    	ADCONbits.EIE = 0;				//Early Interrupt disabled
    	ADCONbits.ORDER = 0;			//Convert even channel first
    	ADCONbits.SEQSAMP = 0;		//Select simultaneous sampling
    	ADCONbits.ADCS = 5;			//ADC clock = FADC/6 = 120MHz / 6 = 20MHz

    	ADPCFGbits.PCFG0 = 0; 		 //select CH0 as analog pin 
    	ADPCFGbits.PCFG1 = 0; 		 //select CH1 as analog pin

    	IFS6bits.ADCP0IF = 0;			//Clear ADC Pair 0 interrupt flag 
    	IPC27bits.ADCP0IP = 5;			//Set ADC Pair 0 interrupt priority
    	IEC6bits.ADCP0IE = 1;			//Enable the ADC Pair 0 interrupt

     	ADSTATbits.P0RDY = 0; 			//Clear Pair 0 data ready bit
    	ADCPC0bits.IRQEN0 = 1;		//Enable ADC Interrupt pair 0 
    	ADCPC0bits.TRGSRC0 = 4; 		//ADC Pair 0 triggered by PWM1 Trigger

	TRGCON1bits.DTM=1;			//dual trigger mode
	TRIG1bits.TRGCMP=0;			//Primary trig compare value 
	STRIG1bits.STRGCMP=0xFFF;	//secondary trig compare value

 	TRGCON1bits.TRGDIV = 0;		// Trigger generated every PWM cycle
	TRGCON1bits.TRGSTRT = 0;		// enable Trigger generated after 0 PWM cycles 
	TRIG1 = 1;						// Trigger compare value
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
