                 Readme File for Code Example:
               CE162 - dsPIC33F SMPS Complementary PWM
               ------------------------------------


This program sets up the PWM module for complementary mode operation. The 
PWM1 and PWM2 outputs are configured to produce a 400kHz PWM signal with  
a duty cycle of 26%. PWM2 is phase shifted by 250 nsec. 

Hardware Dependencies: 16-Bit 28-Pin Starter Board                  

This file provides a brief description of files and folders provided
in the accompanying workspace.

This folder contains the following files:
1. This file
        This file provides a description of the functionality 
	demonstrated by the example source code.

2. Complementary PWM.mcw, Complementary PWM.mcp
        These are MPLAB� IDE workspace and project files that may be
        used to examine, build, program and debug the example source
        code.

3. Complementary.c
        This file is the project source file.
