
#include <xc.h>
#include "pwm.h"

#define PWM_FREQ_KHZ(x)  1021600ull/x  // 4MHZ=63850

#define FREKANS_KHZ 125

void init_PWM() {

    PTPER = PWM_FREQ_KHZ(FREKANS_KHZ);      /* PTPER = ((2.5us) / 1.04ns) = 2404, where 2.5us
                                            is the PWM period and 1.04ns is PWM resolution. */   

    /* ~~~~~~~~~~~~~~~~~~~~~~ PWM2 Configuration ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */    
    IOCON1bits.PENH = 1;   					/* PWM2H is controlled by PWM module */
    IOCON1bits.PENL = 1;   					/* PWM2L is controlled by PWM module */
    IOCON1bits.PMOD = 0;   					/* Select Complementary Output PWM mode */

    PDC1 = PWM_FREQ_KHZ(FREKANS_KHZ)/2;     /* PDC = (625ns / 1.04ns) where 650ns is desired duty cycle */
    PDC1=8000;
    
    DTR1    = PWM_FREQ_KHZ(FREKANS_KHZ)/100;                            /* Deadtime = (65ns / 1.04ns) where 65ns is desired deadtime */
    ALTDTR1 = PWM_FREQ_KHZ(FREKANS_KHZ)/100;                            /* ALTDeadtime = (65ns / 1.04ns) where 65ns is desired deadtime */   
    
    PHASE1 = 0;                             /* Phase shift of 250ns */
    
    
    PTCONbits.PTEN = 1;                     /* Enable the PWM Module */
   
}
