/* 
 * File:   st7565p.h
 * Author: Win7
 *
 * Created on 22 Aral?k 2014 Pazartesi, 15:49
 */



#define ONBINLER 5
#define BINLER 4
#define YUZLER 3
#define ONLAR 2
#define BIRLER 1

#define SYMBOL_KILIT 131
#define SYMBOL_DERECE 130
#define SYMBOL_FAN1 132
#define SYMBOL_FAN2 133
#define SYMBOL_BUZZER1 134
#define SYMBOL_BUZZER2 135



    // Commands
#define ST7565_CMD_DISPLAY_OFF            0xAE
#define ST7565_CMD_DISPLAY_ON             0xAF
#define ST7565_CMD_SET_DISP_START_LINE    0x40
#define ST7565_CMD_SET_PAGE               0xB0
#define ST7565_CMD_SET_COLUMN_UPPER       0x10
#define ST7565_CMD_SET_COLUMN_LOWER       0x00
#define ST7565_CMD_SET_ADC_NORMAL         0xA1
#define ST7565_CMD_SET_ADC_REVERSE        0xA0
#define ST7565_CMD_SET_DISP_NORMAL        0xA6
#define ST7565_CMD_SET_DISP_REVERSE       0xA7
#define ST7565_CMD_SET_ALLPTS_NORMAL      0xA4
#define ST7565_CMD_SET_ALLPTS_ON          0xA5
#define ST7565_CMD_SET_BIAS_9             0xA2
#define ST7565_CMD_SET_BIAS_7             0xA3
#define ST7565_CMD_RMW                    0xE0
#define ST7565_CMD_RMW_CLEAR              0xEE
#define ST7565_CMD_INTERNAL_RESET         0xE2
#define ST7565_CMD_SET_COM_NORMAL         0xC0
#define ST7565_CMD_SET_COM_REVERSE        0xC8
#define ST7565_CMD_SET_POWER_CONTROL      0x28
#define ST7565_CMD_SET_RESISTOR_RATIO     0x20
#define ST7565_CMD_SET_V0_VOLTAGE_REGULATOR 0X81
#define ST7565_CMD_SET_VOLUME_FIRST       0x81
#define ST7565_CMD_SET_VOLUME_SECOND      0
#define ST7565_CMD_SET_STATIC_OFF         0xAC
#define ST7565_CMD_SET_STATIC_ON          0xAD
#define ST7565_CMD_SET_STATIC_REG         0x0
#define ST7565_CMD_SET_BOOSTER_4x     0xF8
#define ST7565_CMD_NOP                    0xE3
#define ST7565_CMD_TEST                   0xF0


#define SATIR1  7
#define SATIR2  15
#define SATIR3  23
#define SATIR4  31
#define SATIR5  39
#define SATIR6  47
#define SATIR7  55
#define SATIR8  63
    //
    // Initialisation/Config Prototypes
    void st7565_Init(void);
    void st7565_Command(uint8_t cc);
    void st7565_Data(uint8_t dd);
    void st7565_SetBrightness(uint8_t val);

    // Drawing Prototypes
    void st7565_ClearScreen(void);
    void st7565_Refresh(void);
    void st7565_DrawLine(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, bool color);
    void st7565_DrawPixel(uint8_t x, uint8_t y, bool color);
    void st7565_DrawPixelInverse(uint8_t x, uint8_t y);
    void st7565_ClearPixel(uint8_t x, uint8_t y);
    void st7565_DrawRectangleFill(int8_t x1, int8_t y1, int8_t x2, int8_t y2, bool color);
    void st7565_DrawRectangle(int8_t x1, int8_t y1, int8_t x2, int8_t y2, bool color);
    void st7565_DrawChar(uint8_t letter, uint8_t x, uint8_t y, uint8_t size, bool color);
    void st7565_DrawChar2(uint8_t letter, uint8_t x, uint8_t y, uint8_t size, bool color);
    void st7565_DrawString(char *str, uint8_t x, uint8_t y, uint8_t size, bool color);
    void st7565_DrawString2(char *str, uint8_t x, uint8_t y, uint8_t size, bool color);
    void st7565_ReverseDisplay();
    void st7565_NormalDisplay();
    void glcd_bar(int x1, int y1, int x2, int y2, int width, bool color);
    void glcd_rect(int8_t x1, int8_t y1, int8_t x2, int8_t y2);
    void st7565_12X16_Draw(uint8_t letter, uint8_t x, uint8_t y,uint8_t color);
    // special function
    uint8_t Mirror(uint8_t value);
    void foo();


