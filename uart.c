#include <xc.h>
#include "uart.h"

#define FP 32000000ul
#define BAUDRATE 115200
#define BRGVAL ((FP/BAUDRATE)/4)-1

void init_UART() {

    U1MODE = 0x0000;
 	U1STA = 0x0000;
    
    _BRGH=1;

 	U1BRG = BRGVAL;
        
	U1MODEbits.UARTEN=1;		// UART1 is enabled
    U1STAbits.UTXEN = 1;           // Initiate transmission

}
