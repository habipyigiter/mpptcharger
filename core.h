/* 
 * File:   core.h
 * Author: User
 *
 * Created on 22 Ekim 2018 Pazartesi, 12:11
 */

#ifndef CORE_H
#define	CORE_H

#ifdef	__cplusplus
extern "C" {
#endif


#define LED1 LATCbits.LATC2
#define LED2 LATCbits.LATC7
#define LED3 LATCbits.LATC8

#define A0          _LATC5
#define CS1         _LATC1
#define RES         _LATC6
#define SPI_SCL     _LATC4
#define SPI_SI      _LATB7



    void CORE_InitCore(void);
    void CORE_InitPorts(void);




#ifdef	__cplusplus
}
#endif

#endif	/* CORE_H */

